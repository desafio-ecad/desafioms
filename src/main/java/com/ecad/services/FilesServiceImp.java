package com.ecad.services;

import com.ecad.domain.File;
import com.ecad.domain.FileType;
import com.ecad.exceptions.BadRequestException;
import com.ecad.repository.FilesRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.List;

@Service
public class FilesServiceImp implements FilesService {

    @Autowired
    private FilesRepository filesRepository;

    @Override
    public Iterable<File> find(final String name, final FileType fileType, final LocalDate createdAtStart, final LocalDate createdAtEnd)
        throws BadRequestException {

        if (StringUtils.isEmpty(name) && fileType == null && createdAtStart == null) {
            return (List<File>) filesRepository.findAll();
        }
        else if (StringUtils.isEmpty(name) && fileType != null && createdAtStart == null) {
            return filesRepository.findByType(fileType);
        }
        else if (StringUtils.isEmpty(name) && fileType == null && createdAtStart != null) {
            return filesRepository.findByCreatedAtBetween(createdAtStart, treatCreatedAtEnd(createdAtStart, createdAtEnd));
        }
        else if (!StringUtils.isEmpty(name) && fileType == null && createdAtStart == null) {
            return filesRepository.findByNameIgnoreCase(name);
        }
        else if (!StringUtils.isEmpty(name) && fileType != null && createdAtStart == null) {
            return filesRepository.findByNameIgnoreCaseAndType(name, fileType);
        }
        else if (!StringUtils.isEmpty(name) && fileType == null && createdAtStart != null) {
            return filesRepository.findByNameIgnoreCaseAndCreatedAtBetween(name, createdAtStart, treatCreatedAtEnd(createdAtStart, createdAtEnd));
        }
        else if (StringUtils.isEmpty(name) && fileType != null && createdAtStart != null) {
            return filesRepository.findByTypeAndCreatedAtBetween(fileType, createdAtStart, treatCreatedAtEnd(createdAtStart, createdAtEnd));
        }
        else {
            return filesRepository.findByNameIgnoreCaseAndTypeAndCreatedAtBetween(name, fileType, createdAtStart,
                treatCreatedAtEnd(createdAtStart, createdAtEnd));
        }
    }

    private LocalDate treatCreatedAtEnd(final LocalDate createdAtStart, final LocalDate createdAtEnd) throws BadRequestException {
        if ((createdAtStart == null && createdAtEnd != null) || (createdAtStart != null && createdAtEnd != null && createdAtStart.compareTo(createdAtEnd) > 0) ) {
            throw new BadRequestException(new String[] { "Invalid start date" });
        }
       

        return (createdAtEnd == null) ? createdAtStart : createdAtEnd;
    }

}
