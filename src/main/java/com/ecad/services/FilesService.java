package com.ecad.services;

import com.ecad.domain.File;
import com.ecad.domain.FileType;
import com.ecad.exceptions.BadRequestException;

import java.time.LocalDate;

public interface FilesService {
    public Iterable<File> find(final String name, final FileType fileType, final LocalDate createdAtStart, final LocalDate createdAtEnd)
        throws BadRequestException;
}
