package com.ecad.controller;

import com.ecad.domain.File;
import com.ecad.domain.FileType;
import com.ecad.services.FilesService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@CrossOrigin
@RestController
@RequestMapping
@Tag(name = "files")
public class FilesController {

    @Autowired
    private FilesService filesService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = File.class))))
    public Iterable<File> find(@RequestParam(value = "name", required = false) final String name,
        @RequestParam(value = "file_type", required = false) final FileType fileType,
        @RequestParam(value = "created_at_start", required = false) @DateTimeFormat(iso = ISO.DATE) final LocalDate createdAtStart,
        @RequestParam(value = "created_at_end", required = false) @DateTimeFormat(iso = ISO.DATE) final LocalDate createdAtEnd) {
        return filesService.find(name, fileType, createdAtStart, createdAtEnd);
    }

}
