package com.ecad.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = -7181078851549125174L;
    private String[] errors;

}
