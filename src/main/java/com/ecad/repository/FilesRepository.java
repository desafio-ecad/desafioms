package com.ecad.repository;

import com.ecad.domain.File;
import com.ecad.domain.FileType;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface FilesRepository extends CrudRepository<File, Long> {

    public Iterable<File> findByNameIgnoreCase(final String name);

    public Iterable<File> findByType(final FileType fileType);

    public Iterable<File> findByCreatedAtBetween(final LocalDate createdAtStart, final LocalDate createdAtEnd);

    public Iterable<File> findByNameIgnoreCaseAndType(final String name, final FileType fileType);

    public Iterable<File> findByNameIgnoreCaseAndCreatedAtBetween(final String name, final LocalDate createdAtStart, final LocalDate createdAtEnd);

    public Iterable<File> findByTypeAndCreatedAtBetween(final FileType fileType, final LocalDate createdAtStart, final LocalDate createdAtEnd);

    public Iterable<File> findByNameIgnoreCaseAndTypeAndCreatedAtBetween(final String name, final FileType fileType,
        final LocalDate createdAtStart, final LocalDate createdAtEnd);

}
