package com.ecad.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_EMPTY)
public class File implements Serializable {
    private static final long serialVersionUID = -2799592047765246241L;

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private LocalDate createdAt;

    @Enumerated(EnumType.STRING)
    private FileType type;

    private Long qtdLines;

    private Double value;

}
