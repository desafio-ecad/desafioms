package com.ecad.domain;

import lombok.Getter;

@Getter
public enum FileType {

    REMESSA, RETORNO

}
