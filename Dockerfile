FROM openjdk:8-jre-alpine
COPY /target/*.jar /usr/local/lib/app.jar
ENTRYPOINT ["java", "-jar", "/usr/local/lib/app.jar"]
 
